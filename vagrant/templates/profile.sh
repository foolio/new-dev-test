# Add Composer vendor binaries to the path
export PATH=$PATH:/vagrant/vendor/bin

# Verbose and colored list output
alias l="ls -lah --color=auto"

# Quickly activate and de-activate xdebug during unit testing
alias xpon="export PHP_IDE_CONFIG="serverName=Vagrant" && export XDEBUG_CONFIG="remote_autostart=1" && echo 'xdebug on'"
alias xpoff="unset PHP_IDE_CONFIG && unset XDEBUG_CONFIG && echo 'xdebug off'"
