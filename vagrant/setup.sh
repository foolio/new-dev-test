#!/bin/bash

# Wipe logs (avoid filling up disk)
echo -n > /vagrant/vagrant/logs/apache.log
echo -n > /vagrant/vagrant/logs/php.log
echo -n > /vagrant/vagrant/logs/opcache.log

# Only do installs if not already done
if [ ! -e /etc/.vagrant-up ]; then

    echo deb http://packages.dotdeb.org jessie all >> /etc/apt/sources.list
    echo deb-src http://packages.dotdeb.org jessie all >> /etc/apt/sources.list

    # Trust new sources
    wget https://www.dotdeb.org/dotdeb.gpg
    apt-key add dotdeb.gpg

    # Update cache
    apt-get update

    # Upgrade all the base base packages
    apt-get upgrade

    # Install Packages
    apt-get -y install \
        vim \
        curl \
        git \
        memcached \
        apache2 \
        apache2-mpm-prefork \
        php7.0 \
        php7.0-curl \
        php7.0-gd \
        php7.0-intl \
        php7.0-ldap \
        php7.0-memcached \
        php7.0-sybase \
        php7.0-xdebug \
        php7.0-mysql \
        php7.0-enchant \
        php7.0-mcrypt \
        php7.0-mbstring \
        php7.0-xml \
        php7.0-soap \
        php7.0-zip \
        freetds-bin \
        openjdk-7-jre-headless \
        mysql-client \
        optipng \
        jpegoptim \
        gifsicle \
        unrtf \
        ant \
        libfontconfig1 \
        libxrender1 \
        ruby-sass

    # LDAP CA
    cp /vagrant/vagrant/templates/NavitasCA.pem /etc/ssl/certs/NavitasCA.pem
    cp /vagrant/vagrant/templates/ldap.conf /etc/ldap/ldap.conf

    # Hosts file
    cat /vagrant/vagrant/templates/hosts >> /etc/hosts

    # Set up browscap
    cp /vagrant/vagrant/templates/php_browscap.ini /etc/php/7.0

    # Set php CLI ini
    cp /vagrant/vagrant/templates/vagrant.ini /etc/php/7.0/mods-available/vagrant.ini
    chmod 644 /etc/php/7.0/mods-available/vagrant.ini
    phpenmod vagrant

    # Zend_Tool
    ln -s /vagrant/vendor/zendframework/zendframework1/bin/zf.sh /usr/local/bin/zf
    cp /vagrant/vagrant/templates/.zf.ini /root
    mkdir /root/.zf
    cp /vagrant/vagrant/templates/.zf.ini /home/vagrant
    chown vagrant:vagrant /home/vagrant/.zf.ini
    mkdir /home/vagrant/.zf
    chown vagrant:vagrant /home/vagrant/.zf

    # Set up SSL
    mkdir -p /etc/apache2/ssl
    cp /vagrant/vagrant/templates/ssl.key /etc/apache2/ssl
    cp /vagrant/vagrant/templates/ssl.pem /etc/apache2/ssl

    # Set up Apache hosts
    cp /vagrant/vagrant/templates/ports.conf /etc/apache2/ports.conf
    cp /vagrant/vagrant/templates/navigate.conf /etc/apache2/sites-available/
    a2ensite navigate.conf
    a2enmod rewrite
    a2enmod headers
    a2enmod ssl
    a2enmod php7.0
    service apache2 restart

    # Set FreeTDS
    cp /vagrant/vagrant/templates/locales.conf /etc/freetds
    cp /vagrant/vagrant/templates/freetds.conf /etc/freetds

    # Set default profile
    cp /vagrant/vagrant/templates/profile.sh /etc/profile.d
    chmod 644 /etc/profile.d/profile.sh

    # Set up composer
    cd /opt
    curl -sS https://getcomposer.org/installer | php
    echo "alias composer='/opt/composer.phar --ansi'" >> /etc/bash.bashrc

    # When logging into vagrant, switch to vagrant folder by default for convenience
    echo "cd /vagrant" >> ~vagrant/.profile
    echo "cd /vagrant" >> ~root/.profile

    # Colours in PHPUnit
    echo "alias phpunit='/vagrant/vendor/bin/phpunit --colors'" >> /etc/bash.bashrc
    echo "alias phing='/vagrant/vendor/bin/phing'" >> /etc/bash.bashrc

    # Colours in Git
    cp /vagrant/vagrant/templates/gitconfig /etc
    chmod 644 /etc/gitconfig

    # Install composer dependencies
    cd /vagrant
    php /opt/composer.phar --ansi --no-interaction --dev install

    # Indicate that setup is complete
    touch /etc/.vagrant-up
fi

service apache2 restart
