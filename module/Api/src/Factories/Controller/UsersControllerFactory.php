<?php

namespace Api\Factories\Controller;

use Api\Controller\UsersController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UsersControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new UsersController(
            $container->get('Doctrine\ORM\EntityManager')
        );
    }
}