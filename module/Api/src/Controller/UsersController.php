<?php

namespace Api\Controller;

use Api\Entity\User;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class UsersController extends AbstractRestfulController
{
    /** @var EntityManager */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getList()
    {
        $hydrator = new DoctrineHydrator($this->entityManager);
        $request = $this->getRequest();

        $users = $this->entityManager->getRepository('\Api\Entity\User')->findAll();

        $extractedUsers = array_map(
            function ($u) use ($hydrator, $request)
            {
                $extractedUser = $hydrator->extract($u);

                $roles = array_map(
                    function ($r) use ($hydrator)
                    {
                        return $hydrator->extract($r);
                    }, $u->getRoles()->toArray()
                );

                $extractedUser['roles'] = $roles;

                $links = [
                    [
                        "rel"   => "self",
                        "href"  => $request->getUriString() . "/" . $u->getId()
                    ]
                ];

                $extractedUser['links'] = $links;

                return $extractedUser;
            }, $users
        );
        return new JsonModel($extractedUsers);
    }

    public function get($id)
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $hydrator = new DoctrineHydrator($this->entityManager);

        $user = $this->entityManager->find('\Api\Entity\User',$id);

        if (is_null($user)) {
            $response->setStatusCode(404);
            return new JsonModel(['error' => 'User Not Found']);
        }

        $extractedUser = $hydrator->extract($user);

        $roles = array_map(
            function ($r) use ($hydrator)
            {
                return $hydrator->extract($r);
            }, $user->getRoles()->toArray()
        );

        $extractedUser['roles'] = $roles;

        $links = [
            [
                "rel"   => "self",
                "href"  => $request->getUriString()
            ]
        ];

        $extractedUser['links'] = $links;

        return new JsonModel($extractedUser);
    }

    public function update($id, $data)
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $hydrator = new DoctrineHydrator($this->entityManager);

        $user = $this->entityManager->find('\Api\Entity\User', $id);

        if (is_null($user)) {
            $response->setStatusCode(404);
            return new JsonModel(['error' => 'User Not Found']);
        }

        $user = $hydrator->hydrate($data, $user);

        if ($user->isValid()) {
            $this->entityManager->merge($user);

            $this->entityManager->flush();

            $response->setStatusCode(200);

            $extractedUser = $hydrator->extract($user);

            $links = [
                [
                    "rel"   => "self",
                    "href"  => $request->getUriString()
                ]
            ];

            $extractedUser['links'] = $links;

           return new JsonModel($extractedUser);
        }

        $response->setStatusCode(400);

        return new JsonModel(['error' => 'Invalid or Missing User Parameters']);
    }

    public function create($data)
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $hydrator = new DoctrineHydrator($this->entityManager);

        $user = new User();

        $user = $hydrator->hydrate($data, $user);

        if ($user->isValid()){
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $response->setStatusCode(201);

            $extractedUser = $hydrator->extract($user);

            $links = [
                [
                    "rel"   => "self",
                    "href"  => $request->getUriString() . "/" . $user->getId()
                ]
            ];

            $extractedUser['links'] = $links;

            return new JsonModel($extractedUser);
        }

        $response->setStatusCode(400);

        return new JsonModel(['error' => 'Invalid or Missing User Parameters']);
    }

    public function delete($id)
    {
        $response = $this->getResponse();

        $user = $this->entityManager->find('\Api\Entity\User', $id);

        if (is_null($user)){
            $response->setStatusCode(404);
            return new JsonModel(['error' => 'User Not Found']);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        $response->setStatusCode(200);

        return new JsonModel();
    }
}
