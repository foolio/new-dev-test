<?php

namespace Api;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $sharedEvents = $event->getApplication()->getEventManager()->getSharedManager();

        $sharedEvents->attach(__NAMESPACE__, 'dispatch',
            function ($e)
            {
                $log = new Logger('requests');
                $log->pushHandler(new StreamHandler('data/logs/requests.log', Logger::INFO));

                $log->info($e->getRequest());
            }
        );
    }
}