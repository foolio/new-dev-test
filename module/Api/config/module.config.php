<?php

namespace Api;

use Api\Factories\Controller\UsersControllerFactory;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            'api_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Api\Entity' => 'api_entities'
                ]
            ]
        ]
    ],
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/api',
                    'defaults' => [
                        'controller' => Controller\IndexController::class
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'users' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/users[/:id]',
                            'constraints' => [
                                'id' => '[0-9]*'
                            ],
                            'defaults' => [
                                'controller' => Controller\UsersController::class
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\UsersController::class => UsersControllerFactory::class
        ]
    ]
];
