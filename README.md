# Task Requirements

Congratulations on making it this far in the interview process!

The primary purpose of this test to create a simple CRUD API using Zend Framework.

You should not spend any more than two hours completing this test. The aim is to try and complete as many tasks as you can in this time.

### Things to keep in mind while completing this exercise -
- You MUST use PSR standards while completing this task
    * More info can be found here - http://www.php-fig.org/psr/
- All db related exercise is done using SQLite and the database file is located at data/sqlite/navitas.db
- You are welcome to ask a Navitas developer for help if you get stuck or need clarification.
- You are allowed to Google any solutions and we encourage it!
- Do not jump between tasks. Complete one task before moving onto the next task.
- The whole exercise will be done in JSON only
- After completing each task you need to commit the task into git with a message (except task 1)
    * Example commit message :- Task-2 : Installed the latest version of Monolog
    * You are welcome to have multiple commits per task but you have to put task number in each commit message
- Remember that you are using Zend Framework and it is expected that you complete the tasks in the "Zend Framework" way
- Zend Framework 1 (ZF1) is not the same as ZF2 or greater. ZF2 and greater is a complete rewrite and ZF3 is mostly backwards compatible with ZF2
- The main module you will be working on is Api which can be found in module/Api/
- Only complete bonus tasks if you have finished all the required tasks.
- The provided SQLite Datbase has the following -
~~~~
DROP TABLE IF EXISTS User;

CREATE TABLE User (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username VARCHAR(255) NOT NULL,
  firstName VARCHAR(255) NOT NULL,
  lastName VARCHAR(255) NOT NULL,
  state BOOLEAN NOT NULL
);

INSERT INTO User(username, firstname, lastname, state) VALUES
  ('james.bond', 'James', 'Bond', 0),
  ('homer.simpson', 'Homer', 'Simpson', 1),
  ('phillp.fry', 'Phillip', 'Fry', 1),
  ('edna.mode', 'Edna', 'Mode', 1),
  ('harry.potter', 'Harry', 'Potter', 0),
  ('obi-wan.kenobi', 'Obi-Wan', 'Kenobi', 0),
  ('luke.skywalker', 'Luke', 'Skywalker', 0),
  ('doc.brown', 'Doc', 'Brown', 0),
  ('the.joker', 'The', 'Joker', 0),
  ('indiana.jones', 'Indiana', 'Jones', 0);

DROP TABLE IF EXISTS Role;

CREATE TABLE Role (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(255) NOT NULL
);

INSERT INTO Role(name) VALUES
  ('CEO'),
  ('Not a CEO');
~~~~

### Tasks -

1. Setup your development environment
    * Run "composer install" to install all the dependencies
    * A fully configured vagrant & docker file is provided if you wish to use them. You can access them by going to http://localhost:8083/api/users
    * You can refer to README-SETUP.md for additional help

2. Create a new working branch from the master branch. The branch name should be in the following format "feature/<firstname>.<lastname>". You will commit all your work in this new branch.
    * If your name is Foo Bar, then the branch name you will create is feature/foo.bar
    * HINT : https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging#Basic-Branching

3. Implement GET on Users Api. When accessing /api/users, I want a list of all the users that doctrine provides. If I go to /api/users/1, then I want the user record of the user with id 1
    * You will need to update the route on the router to accept 'userId' and ensure it is an integer, it should error otherwise
    * You will need pull data direct from Doctrine and present this in JSON
    * User entity is located at module/Api/src/Entity
    * There should be 10 users by default
    * HINT : https://framework.zend.com/manual/2.1/en/modules/zend.mvc.routing.html#zend-mvc-router-http-segment
    * HINT : http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#entities-and-the-identity-map
    * HINT : https://github.com/doctrine/DoctrineModule/blob/master/docs/hydrator.md#example-1--simple-entity-with-no-associations

4. Implement POST & PUT on Users API
    * Input will be in JSON (in the same format as GET)
    * Ensure PUT is idempotent
    * Ensure appropriate HTTP status codes are used
    * You will need to ensure that the user data is valid
    * Refer to Zend\Mvc\Controller\AbstractRestfulController::onBootstrap for hints on how to implement this
    * HINT : http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#persisting-entities

5. Implement DELETE on Users API
    * Ensure appropriate HTTP status codes are used
    * HINT : http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#removing-entities

6. Tell Doctrine about the one to many relationship between User & Role
    * Create a new property in the user entity called roles and update the annotations
    * Emphasis needs to be put on "Normalising" the relationship
    * Update GET methods to return all the roles that is associated with the user
    * HINT : http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/association-mapping.html#one-to-many-unidirectional-with-join-table
    * HINT : https://github.com/doctrine/DoctrineModule/blob/master/docs/hydrator.md#example-2--onetoonemanytoone-associations

7. Install Monolog using Composer and ensure all inbound requests are logged to a text file
    * If you have never used composer before, you can find some info here - https://getcomposer.org/doc/03-cli.md#require
    * Monolog project - https://github.com/Seldaek/monolog
    * Logs must be placed in data/logs
    * HINT : Instead of adding in code in each action, lookup Zend's Event Manager and attach to an appropriate event that is fired by the controller automatically. The actual logging code can live inside a ZF "service"

8. ** BONUS ** Update all the APIs so that it HATEOAS compliant

9. ** BONUS ** Instead of returning HTML, return JSON when Content-Type is application/json

10. ** BONUS ** Write Unit tests
